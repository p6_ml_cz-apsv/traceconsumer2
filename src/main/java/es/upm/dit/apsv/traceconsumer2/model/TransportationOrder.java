package es.upm.dit.apsv.traceconsumer2.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TransportationOrder {
    
    private String toid;
    @Id
    private String truck;

    private long originDate;
    private double originLat;
    private double originLong;

    private long dstDate;
    private double dstLat;
    private double dstLong;

    private long lastDate;
    private double lastLat;
    private double lastLong;
    
    private int st;

    public TransportationOrder() {
    }

    public TransportationOrder(String toid, String truck, long originDate, double originLat, double originLong,
            long dstDate, double dstLat, double dstLong, long lastDate, double lastLat, double lastLong, int st) {
        this.toid = toid;
        this.truck = truck;
        this.originDate = originDate;
        this.originLat = originLat;
        this.originLong = originLong;
        this.dstDate = dstDate;
        this.dstLat = dstLat;
        this.dstLong = dstLong;
        this.lastDate = lastDate;
        this.lastLat = lastLat;
        this.lastLong = lastLong;
        this.st = st;
    }

    public String getToid() {
        return toid;
    }

    public void setToid(String toid) {
        this.toid = toid;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public long getOriginDate() {
        return originDate;
    }

    public void setOriginDate(long originDate) {
        this.originDate = originDate;
    }

    public double getOriginLat() {
        return originLat;
    }

    public void setOriginLat(double originLat) {
        this.originLat = originLat;
    }

    public double getOriginLong() {
        return originLong;
    }

    public void setOriginLong(double originLong) {
        this.originLong = originLong;
    }

    public long getDstDate() {
        return dstDate;
    }

    public void setDstDate(long dstDate) {
        this.dstDate = dstDate;
    }

    public double getDstLat() {
        return dstLat;
    }

    public void setDstLat(double dstLat) {
        this.dstLat = dstLat;
    }

    public double getDstLong() {
        return dstLong;
    }

    public void setDstLong(double dstLong) {
        this.dstLong = dstLong;
    }

    public long getLastDate() {
        return lastDate;
    }

    public void setLastDate(long lastDate) {
        this.lastDate = lastDate;
    }

    public double getLastLat() {
        return lastLat;
    }

    public void setLastLat(double lastLat) {
        this.lastLat = lastLat;
    }

    public double getLastLong() {
        return lastLong;
    }

    public void setLastLong(double lastLong) {
        this.lastLong = lastLong;
    }

    public int getSt() {
        return st;
    }

    public void setSt(int st) {
        this.st = st;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (dstDate ^ (dstDate >>> 32));
        long temp;
        temp = Double.doubleToLongBits(dstLat);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(dstLong);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + (int) (lastDate ^ (lastDate >>> 32));
        temp = Double.doubleToLongBits(lastLat);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lastLong);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + (int) (originDate ^ (originDate >>> 32));
        temp = Double.doubleToLongBits(originLat);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(originLong);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + st;
        result = prime * result + ((toid == null) ? 0 : toid.hashCode());
        result = prime * result + ((truck == null) ? 0 : truck.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransportationOrder other = (TransportationOrder) obj;
        if (dstDate != other.dstDate)
            return false;
        if (Double.doubleToLongBits(dstLat) != Double.doubleToLongBits(other.dstLat))
            return false;
        if (Double.doubleToLongBits(dstLong) != Double.doubleToLongBits(other.dstLong))
            return false;
        if (lastDate != other.lastDate)
            return false;
        if (Double.doubleToLongBits(lastLat) != Double.doubleToLongBits(other.lastLat))
            return false;
        if (Double.doubleToLongBits(lastLong) != Double.doubleToLongBits(other.lastLong))
            return false;
        if (originDate != other.originDate)
            return false;
        if (Double.doubleToLongBits(originLat) != Double.doubleToLongBits(other.originLat))
            return false;
        if (Double.doubleToLongBits(originLong) != Double.doubleToLongBits(other.originLong))
            return false;
        if (st != other.st)
            return false;
        if (toid == null) {
            if (other.toid != null)
                return false;
        } else if (!toid.equals(other.toid))
            return false;
        if (truck == null) {
            if (other.truck != null)
                return false;
        } else if (!truck.equals(other.truck))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TrasnportationOrder [dstDate=" + dstDate + ", dstLat=" + dstLat + ", dstLong=" + dstLong + ", lastDate="
                + lastDate + ", lastLat=" + lastLat + ", lastLong=" + lastLong + ", originDate=" + originDate
                + ", originLat=" + originLat + ", originLong=" + originLong + ", st=" + st + ", toid=" + toid
                + ", truck=" + truck + "]";
    }

    public double distanceToDestination() {
        return Math.sqrt(Math.pow(this.dstLat -this.lastLat, 2)
                    + Math.pow(this.dstLong - this.lastLong, 2));
    }

}
